From: Yogeswaran Umasankar <kd8mbd@gmail.com>
Date: Fri, 20 Dec 2024 02:33:59 +0000
Subject: Replace pytz with python build-in module.

Last-Update: 2024-05-26
Forwarded: https://github.com/googleapis/proto-plus-python/issues/450

pytz has a non-standard interface that is very easy to misuse.
Replaced pytz with python build-in datetime module.
---
 tests/test_datetime_helpers.py | 48 +++++++++++++++++++++---------------------
 1 file changed, 24 insertions(+), 24 deletions(-)

diff --git a/tests/test_datetime_helpers.py b/tests/test_datetime_helpers.py
index 264b529..a07a607 100644
--- a/tests/test_datetime_helpers.py
+++ b/tests/test_datetime_helpers.py
@@ -13,10 +13,9 @@
 # limitations under the License.
 
 import calendar
-import datetime
+from datetime import datetime, timedelta, timezone
 
 import pytest
-import pytz
 
 from proto import datetime_helpers
 from google.protobuf import timestamp_pb2
@@ -26,9 +25,10 @@ ONE_MINUTE_IN_MICROSECONDS = 60 * 1e6
 
 
 def test_from_microseconds():
+    ONE_MINUTE_IN_MICROSECONDS = 60 * 1000000
     five_mins_from_epoch_in_microseconds = 5 * ONE_MINUTE_IN_MICROSECONDS
-    five_mins_from_epoch_datetime = datetime.datetime(
-        1970, 1, 1, 0, 5, 0, tzinfo=datetime.timezone.utc
+    five_mins_from_epoch_datetime = datetime(
+        1970, 1, 1, 0, 5, 0, tzinfo=timezone.utc
     )
 
     result = datetime_helpers._from_microseconds(five_mins_from_epoch_in_microseconds)
@@ -37,27 +37,27 @@ def test_from_microseconds():
 
 
 def test_to_rfc3339():
-    value = datetime.datetime(2016, 4, 5, 13, 30, 0)
+    value = datetime(2016, 4, 5, 13, 30, 0)
     expected = "2016-04-05T13:30:00.000000Z"
     assert datetime_helpers._to_rfc3339(value) == expected
 
 
 def test_to_rfc3339_with_utc():
-    value = datetime.datetime(2016, 4, 5, 13, 30, 0, tzinfo=datetime.timezone.utc)
+    value = datetime(2016, 4, 5, 13, 30, 0, tzinfo=timezone.utc)
     expected = "2016-04-05T13:30:00.000000Z"
     assert datetime_helpers._to_rfc3339(value, ignore_zone=False) == expected
 
 
 def test_to_rfc3339_with_non_utc():
-    zone = pytz.FixedOffset(-60)
-    value = datetime.datetime(2016, 4, 5, 13, 30, 0, tzinfo=zone)
+    zone = timezone(timedelta(minutes=-60))
+    value = datetime(2016, 4, 5, 13, 30, 0, tzinfo=zone)
     expected = "2016-04-05T14:30:00.000000Z"
     assert datetime_helpers._to_rfc3339(value, ignore_zone=False) == expected
 
 
 def test_to_rfc3339_with_non_utc_ignore_zone():
-    zone = pytz.FixedOffset(-60)
-    value = datetime.datetime(2016, 4, 5, 13, 30, 0, tzinfo=zone)
+    zone = timezone(timedelta(minutes=-60))
+    value = datetime(2016, 4, 5, 13, 30, 0, tzinfo=zone)
     expected = "2016-04-05T13:30:00.000000Z"
     assert datetime_helpers._to_rfc3339(value, ignore_zone=True) == expected
 
@@ -149,7 +149,7 @@ def test_from_rfc3339_w_invalid():
 def test_from_rfc3339_wo_fraction():
     timestamp = "2016-12-20T21:13:47Z"
     expected = datetime_helpers.DatetimeWithNanoseconds(
-        2016, 12, 20, 21, 13, 47, tzinfo=datetime.timezone.utc
+        2016, 12, 20, 21, 13, 47, tzinfo=timezone.utc
     )
     stamp = datetime_helpers.DatetimeWithNanoseconds.from_rfc3339(timestamp)
     assert stamp == expected
@@ -158,7 +158,7 @@ def test_from_rfc3339_wo_fraction():
 def test_from_rfc3339_w_partial_precision():
     timestamp = "2016-12-20T21:13:47.1Z"
     expected = datetime_helpers.DatetimeWithNanoseconds(
-        2016, 12, 20, 21, 13, 47, microsecond=100000, tzinfo=datetime.timezone.utc
+        2016, 12, 20, 21, 13, 47, microsecond=100000, tzinfo=timezone.utc
     )
     stamp = datetime_helpers.DatetimeWithNanoseconds.from_rfc3339(timestamp)
     assert stamp == expected
@@ -167,7 +167,7 @@ def test_from_rfc3339_w_partial_precision():
 def test_from_rfc3339_w_full_precision():
     timestamp = "2016-12-20T21:13:47.123456789Z"
     expected = datetime_helpers.DatetimeWithNanoseconds(
-        2016, 12, 20, 21, 13, 47, nanosecond=123456789, tzinfo=datetime.timezone.utc
+        2016, 12, 20, 21, 13, 47, nanosecond=123456789, tzinfo=timezone.utc
     )
     stamp = datetime_helpers.DatetimeWithNanoseconds.from_rfc3339(timestamp)
     assert stamp == expected
@@ -195,7 +195,7 @@ def test_from_rfc3339_test_nanoseconds(fractional, nanos):
 
 def test_timestamp_pb_wo_nanos_naive():
     stamp = datetime_helpers.DatetimeWithNanoseconds(2016, 12, 20, 21, 13, 47, 123456)
-    delta = stamp.replace(tzinfo=datetime.timezone.utc) - datetime_helpers._UTC_EPOCH
+    delta = stamp.replace(tzinfo=timezone.utc) - datetime_helpers._UTC_EPOCH
     seconds = int(delta.total_seconds())
     nanos = 123456000
     timestamp = timestamp_pb2.Timestamp(seconds=seconds, nanos=nanos)
@@ -204,7 +204,7 @@ def test_timestamp_pb_wo_nanos_naive():
 
 def test_timestamp_pb_w_nanos():
     stamp = datetime_helpers.DatetimeWithNanoseconds(
-        2016, 12, 20, 21, 13, 47, nanosecond=123456789, tzinfo=datetime.timezone.utc
+        2016, 12, 20, 21, 13, 47, nanosecond=123456789, tzinfo=timezone.utc
     )
     delta = stamp - datetime_helpers._UTC_EPOCH
     timestamp = timestamp_pb2.Timestamp(
@@ -214,8 +214,8 @@ def test_timestamp_pb_w_nanos():
 
 
 def test_from_timestamp_pb_wo_nanos():
-    when = datetime.datetime(
-        2016, 12, 20, 21, 13, 47, 123456, tzinfo=datetime.timezone.utc
+    when = datetime(
+        2016, 12, 20, 21, 13, 47, 123456, tzinfo=timezone.utc
     )
     delta = when - datetime_helpers._UTC_EPOCH
     seconds = int(delta.total_seconds())
@@ -226,12 +226,12 @@ def test_from_timestamp_pb_wo_nanos():
     assert _to_seconds(when) == _to_seconds(stamp)
     assert stamp.microsecond == 0
     assert stamp.nanosecond == 0
-    assert stamp.tzinfo == datetime.timezone.utc
+    assert stamp.tzinfo == timezone.utc
 
 
 def test_replace():
     stamp = datetime_helpers.DatetimeWithNanoseconds(
-        2016, 12, 20, 21, 13, 47, 123456, tzinfo=datetime.timezone.utc
+        2016, 12, 20, 21, 13, 47, 123456, tzinfo=timezone.utc
     )
 
     # ns and ms provided raises
@@ -261,8 +261,8 @@ def test_replace():
 
 
 def test_from_timestamp_pb_w_nanos():
-    when = datetime.datetime(
-        2016, 12, 20, 21, 13, 47, 123456, tzinfo=datetime.timezone.utc
+    when = datetime(
+        2016, 12, 20, 21, 13, 47, 123456, tzinfo=timezone.utc
     )
     delta = when - datetime_helpers._UTC_EPOCH
     seconds = int(delta.total_seconds())
@@ -273,17 +273,17 @@ def test_from_timestamp_pb_w_nanos():
     assert _to_seconds(when) == _to_seconds(stamp)
     assert stamp.microsecond == 123456
     assert stamp.nanosecond == 123456789
-    assert stamp.tzinfo == datetime.timezone.utc
+    assert stamp.tzinfo == timezone.utc
 
 
 def _to_seconds(value):
     """Convert a datetime to seconds since the unix epoch.
 
     Args:
-        value (datetime.datetime): The datetime to convert.
+        value (datetime): The datetime to convert.
 
     Returns:
         int: Microseconds since the unix epoch.
     """
-    assert value.tzinfo is datetime.timezone.utc
+    assert value.tzinfo is timezone.utc
     return calendar.timegm(value.timetuple())
